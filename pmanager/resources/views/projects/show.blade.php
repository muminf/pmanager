@extends('layouts.app')

@section('content')


  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="col-sm-9 col-md-9 col-lg-9 pull-left">
  <div class="well well-lg">
      <h1>{{ $project->name }}</h1>
      <p>{{ $project->description }}</p>
      <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p> -->
  </div>
    <!-- Example row of columns -->
    <div class="row" style="background:white; margin:10px">
    <a href="/projects/create" class="pull-right btn btn-primary btn-sm">Add project</a>
    
    
   <br/> 
   @include('partials.comments')
  <div class="row container-fluid">
    <form method="post" action="{{ route('comments.store')}}">
        {{ csrf_field() }}
        <input type="hidden" name="commentable_type" value="App\Project">
        <input type="hidden" name="commentable_id" value="{{ $project->id}}">
        <div class="form-group">
            <label for="comment-description">Comment <span class="required">*</span> </label>
            <textarea placeholder="Enter comment"
                   style="resize: vertical"
                   id="comment-content"
                   name="body"
                   rows="5" spellcheck="false"
                   class="form-control autosize-target text-left"
                   ></textarea>
        </div>
        <div class="form-group">
            <label for="comment-description">Proof of work done (Url/Photos) <span class="required">*</span> </label>
            <textarea placeholder="Enter url or screenshots"
                   style="resize: vertical"
                   id="comment-content"
                   name="url"
                   rows="3" spellcheck="false"
                   class="form-control autosize-target text-left"
                   ></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit"/>
        </div>
        </form>
      </div>
    
    
     </div>
    
</div>
    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
       <div class="p-3">
        <h4 class="font-italic">Actions</h4>
        <ol class="list-unstyled">
          <li><a href="/projects/create">Add project</a></li>
          <li><a href="/projects">My projects</a></li>
          @if($project->user_id == Auth::user()->id)
            <li><a href="/projects/{{ $project->id }}/edit">Edit</a></li>
              <li>
                <a 
                href="#"
                  onclick="
                    var result = confirm('Хотите удалить?');
                      if(result){
                        event.preventDefault();
                        document.getElementById('logout-form').submit();
                      }
                  "
                >Delete</a>
                <form method="post" id="logout-form" action="{{ route('projects.destroy',[$project->id])}}" style="display:none;">
                      <input type="hidden" name="_method" value="delete">
                      {{ csrf_field() }}
              </form>
            </li>
          @endif
        </ol>
        <hr>
        <div class="p-3">
          <h4 class="font-italic">Add members</h4>
          <form method="post" action="{{ route('projects.adduser')}}">
            <div class="input-group">
              <input type="hidden" name="project_id" value="{{ $project->id }}">
              <input type="text" name="email" class="form-control" placeholder="Email">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">Add!</button>
              </span>
            </div>
            {{ csrf_field() }}
          </form>
        </div>
        <div class="p-3">
          <h4 class="font-italic">Team members</h4>
          <ol class="list-unstyled">
              @foreach($project->users as $user)
                  <li><a href="#">{{ $user->name }}</a></li>
              @endforeach
          </ol>
        </div>
      </div>
    </div>

  </div> <!-- /container -->


@endsection