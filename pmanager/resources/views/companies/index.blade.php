@extends('layouts.app')

@section('content')

<div class="col-md-6 col-lg-6 col-md-offset-2  col-lg-offset-3">
    <div class="panel panel-primary">
    <div class="panel-heading"><a class="pull-right btn btn-primary btn-sm" href="/companies/create">Create new company</a></div>
    <div class="panel-body">
    
    <ul class="list-group">
    <li class="list-group-item active">Companies</li>
        @foreach($companies as $company)
            <li class="list-group-item"><a href="/companies/{{ $company->id }}">{{ $company->name }}</a></li>
        @endforeach
    </ul>
    </div>
    </div>
</div>
    
@endsection