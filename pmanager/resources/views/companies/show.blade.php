@extends('layouts.app')

@section('content')


  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="col-sm-9 col-md-9 col-lg-9 pull-left">
  <div class="jumbotron">
      <h1>{{ $company->name }}</h1>
      <p>{{ $company->description }}</p>
      <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p> -->
  </div>
    <!-- Example row of columns -->
    <div class="row" style="background:white; margin:10px">
    <a href="/projects/create/{{ $company->id }}" class="pull-right btn btn-primary btn-sm">Add project</a>
    @foreach($company->projects as $project)
      <div class="col-md-4">
        <h2>{{ $project->name }}</h2>
        <p>{{ $project->description }}</p>
        <p><a class="btn btn-primary" href="/projects/{{ $project->id }}" role="button">View project »</a></p>
      </div>
    @endforeach
    </div>
</div>
    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
       <div class="p-3">
        <h4 class="font-italic">Actions</h4>
        <ol class="list-unstyled">
          <li><a href="/companies/{{ $company->id }}/edit">Edit</a></li>
          <li>
            <a 
            href="#"
              onclick="
                var result = confirm('Хотите удалить?');
                  if(result){
                    event.preventDefault();
                    document.getElementById('logout-form').submit();
                  }
              "
            >Delete</a>
            <form method="post" id="logout-form" action="{{ route('companies.destroy',[$company->id])}}" style="display:none;">
                  <input type="hidden" name="_method" value="delete">
                  {{ csrf_field() }}
            </form>
          </li>
          <li><a href="/projects/create/{{ $company->id }}">Add project</a></li>
          <li><a href="/companies">My Companies</a></li>
          <li><a href="/companies/create">Create new Company</a></li>
        </ol>
      </div>
    </div>

  </div> <!-- /container -->


@endsection